﻿using Microsoft.Extensions.DependencyInjection;
using R.Demo.Command;
using R.Demo.Command.Movement;
using R.Demo.Interfaces;

namespace R.Demo.Dependency
{
    public class DependencyRegister
    {
        public static ServiceProvider Register()
        {
            return new ServiceCollection()
                .AddScoped<ICommandInterpreter, CommandInterpreter>()
                .AddScoped<ICommandExecuter, NorthCommandExecuter>()
                .AddScoped<ICommandExecuter, EastCommandExecuter>()
                .AddScoped<ICommandExecuter, WestCommandExecuter>()
                .AddScoped<ICommandExecuter, SouthCommandExecuter>()
                .AddScoped<IRover, Rover>()
                .BuildServiceProvider();
        }
    }
}
