﻿using System.Collections.Generic;
using R.Demo.Models;

namespace R.Demo.Interfaces
{
    public interface IRover
    {
        Position Move(List<RoverCommand> listOfCommands);
    }
}
