﻿using R.Demo.Models;

namespace R.Demo.Interfaces
{
    public interface ICommandExecuter
    {
        FaceDirection DirectionOfMovement { get; }
        void ExecuteCommand(RoverCommand roverCommand, Position position);
    }
}
