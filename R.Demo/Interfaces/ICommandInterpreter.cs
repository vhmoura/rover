﻿using System.Collections.Generic;
using R.Demo.Models;

namespace R.Demo.Interfaces
{
    public interface ICommandInterpreter
    {
        List<RoverCommand> ProcessInput(string command);
    }
}
