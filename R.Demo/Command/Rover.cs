﻿using System.Collections.Generic;
using System.Linq;
using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.Command
{
    public class Rover: IRover
    {
        private readonly IEnumerable<ICommandExecuter> _commandExecuters;
        private Position _position = new Position();

        public Rover(IEnumerable<ICommandExecuter> commandExecuters)
        {
            _commandExecuters = commandExecuters;
        }

        public Position Move(List<RoverCommand> listOfCommands)
        {
            foreach (var command in listOfCommands)
            {
                var directionToGo =
                    _commandExecuters.FirstOrDefault(r => r.DirectionOfMovement == command.FaceDirection);

                directionToGo?.ExecuteCommand(command, _position);
            }

            return _position;
        }
    }
}
