﻿using System;
using System.Collections.Generic;
using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.Command
{
    public class CommandInterpreter: ICommandInterpreter
    {
        public List<RoverCommand> ProcessInput(string command)
        {
            if (string.IsNullOrWhiteSpace(command)) 
                throw new ArgumentException("Invalid command");

            return new List<RoverCommand>
            {
                new RoverCommand(),
                new RoverCommand(),
                new RoverCommand(),
                new RoverCommand()
            };
        }
    }
}
