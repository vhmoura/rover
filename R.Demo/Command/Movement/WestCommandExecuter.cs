﻿using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.Command.Movement
{
    public class WestCommandExecuter: ICommandExecuter
    {
        public FaceDirection DirectionOfMovement => FaceDirection.West;
        public void ExecuteCommand(RoverCommand roverCommand, Position position)
        {
            position.FaceDirection = FaceDirection.West;
            switch (roverCommand.CommandDirection)
            {
                case CommandDirection.Forward:
                    position.X = position.X - 1;
                    break;
                case CommandDirection.Backward:
                    position.X = position.X + 1;
                    break;
            }
        }
    }
}
