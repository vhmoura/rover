﻿using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.Command.Movement
{
    public class NorthCommandExecuter: ICommandExecuter
    {
        public FaceDirection DirectionOfMovement => FaceDirection.North;

        public void ExecuteCommand(RoverCommand roverCommand, Position position)
        {
            position.FaceDirection = FaceDirection.North;

            switch (roverCommand.CommandDirection)
            {
                case CommandDirection.Forward:
                    position.Y = position.Y + 1;
                    break;
                case CommandDirection.Backward:
                    position.Y = position.Y - 1;
                    break;
            }

        }
    }
}
