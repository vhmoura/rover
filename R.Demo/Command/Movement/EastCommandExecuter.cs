﻿using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.Command.Movement
{
    public class EastCommandExecuter: ICommandExecuter
    {
        public FaceDirection DirectionOfMovement => FaceDirection.East;

        public void ExecuteCommand(RoverCommand roverCommand, Position position)
        {
            position.FaceDirection = FaceDirection.East;
            switch (roverCommand.CommandDirection)
            {
                case CommandDirection.Forward:
                    position.X = position.X + 1;
                    break;
                case CommandDirection.Backward:
                    position.X = position.X - 1;
                    break;
            }
        }
    }
}
