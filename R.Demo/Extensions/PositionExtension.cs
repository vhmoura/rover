﻿using System;
using R.Demo.Models;

namespace R.Demo.Extensions
{
    public static class PositionExtension
    {
        public static string FinalPosition(this Position position)
        {
            return $"The absolute position is {Math.Abs(position.X)} and {Math.Abs(position.Y)} and cartesian coordinate is ({position.X},{position.Y}) facing {position.FaceDirection}";
        }
    }
}
