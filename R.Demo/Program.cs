﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using R.Demo.Dependency;
using R.Demo.Extensions;
using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = DependencyRegister.Register();

            //var commandInterpreter = serviceProvider.GetService<ICommandInterpreter>();
            //commandInterpreter.ProcessInput(Console.ReadLine());

            var listOfCommands = new List<RoverCommand>
            {
                new RoverCommand {CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North},
                new RoverCommand {CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North},
                new RoverCommand {CommandDirection = CommandDirection.Backward, FaceDirection = FaceDirection.East},
                new RoverCommand {CommandDirection = CommandDirection.Backward, FaceDirection = FaceDirection.East},
            };

            var rover = serviceProvider.GetService<IRover>();
            var finalPosition = rover.Move(listOfCommands);
            
            Console.WriteLine(finalPosition.FinalPosition());

            Console.ReadKey();
        }
    }
}
