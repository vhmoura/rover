﻿namespace R.Demo.Models
{
    public enum FaceDirection
    {
        North,
        South,
        East,
        West
    }
}
