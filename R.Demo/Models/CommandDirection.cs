﻿namespace R.Demo.Models
{
    public enum CommandDirection
    {
        Forward,
        Backward,
        Left,
        Right
    }
}
