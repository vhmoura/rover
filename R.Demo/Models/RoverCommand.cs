﻿namespace R.Demo.Models
{
    public class RoverCommand
    {
        public CommandDirection CommandDirection { get; set; }
        public FaceDirection FaceDirection { get; set; }
    }
}
