﻿namespace R.Demo.Models
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public FaceDirection FaceDirection { get; set; }
    }
}
