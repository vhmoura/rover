﻿using System.Collections.Generic;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R.Demo.Command;
using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.UnitTest.Command
{
    [TestClass]
    public class RoverTests
    {
        [TestMethod]
        public void Class_InheritsFromInterface()
        {
            typeof(Rover).Should().BeAssignableTo<IRover>();
        }

        [TestMethod]
        public void FakeRover_Move_ShouldMoveToExpectedPosition()
        {
            var fakeRover = A.Fake<IRover>();
            var position = new Position {X = 2, Y = 2};
            var listOfCommands = new List<RoverCommand>
            {
                new RoverCommand {CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North},
                new RoverCommand {CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North},
                new RoverCommand {CommandDirection = CommandDirection.Backward, FaceDirection = FaceDirection.East},
                new RoverCommand {CommandDirection = CommandDirection.Backward, FaceDirection = FaceDirection.East},
            };

            A.CallTo(() => fakeRover.Move(A<List<RoverCommand>>.Ignored)).Returns(position);

            var finalPosition = fakeRover.Move(listOfCommands);

            finalPosition.Should().NotBeNull();
            finalPosition.X.Should().Be(2);
            finalPosition.Y.Should().Be(2);
        }
    }
}
