﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R.Demo.Command.Movement;
using R.Demo.Interfaces;
using R.Demo.Models;

namespace R.Demo.UnitTest.Command
{
    [TestClass]
    public class CommandExecuterTests
    {
        [TestMethod]
        public void NorthCommand_InheritsFromInterface()
        {
            typeof(NorthCommandExecuter).Should().BeAssignableTo<ICommandExecuter>();
        }

        [TestMethod]
        public void SouthCommand_InheritsFromInterface()
        {
            typeof(SouthCommandExecuter).Should().BeAssignableTo<ICommandExecuter>();
        }

        [TestMethod]
        public void WestCommand_InheritsFromInterface()
        {
            typeof(WestCommandExecuter).Should().BeAssignableTo<ICommandExecuter>();
        }

        [TestMethod]
        public void EastCommand_InheritsFromInterface()
        {
            typeof(EastCommandExecuter).Should().BeAssignableTo<ICommandExecuter>();
        }

        [TestMethod]
        public void NorthCommand_ShouldFaceNorth()
        {
            var northCommandExecuter = new NorthCommandExecuter();
            var roverCommand = new RoverCommand
                {CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North};
            var position = new Position();

            northCommandExecuter.ExecuteCommand(roverCommand, position);

            position.FaceDirection.Should().Be(FaceDirection.North);
        }

        [TestMethod]
        public void NorthCommand_WhenMovingForward_ShouldIncreaseYPosition()
        {
            var northCommandExecuter = new NorthCommandExecuter();
            var roverCommand = new RoverCommand
                { CommandDirection = CommandDirection.Forward, FaceDirection = FaceDirection.North };
            var position = new Position {X = 1, Y = 1};

            northCommandExecuter.ExecuteCommand(roverCommand, position);

            position.FaceDirection.Should().Be(FaceDirection.North);
            position.Y.Should().Be(2);
        }

        [TestMethod]
        public void NorthCommand_WhenMovingBackward_ShouldDecreaseYPosition()
        {
            var northCommandExecuter = new NorthCommandExecuter();
            var roverCommand = new RoverCommand
                { CommandDirection = CommandDirection.Backward, FaceDirection = FaceDirection.North };
            var position = new Position { X = 1, Y = 5 };

            northCommandExecuter.ExecuteCommand(roverCommand, position);

            position.FaceDirection.Should().Be(FaceDirection.North);
            position.Y.Should().Be(4);
        }
    }

}
