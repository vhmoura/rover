﻿using System;
using System.Collections.Generic;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R.Demo.Command;
using R.Demo.Interfaces;
using R.Demo.Models;


namespace R.Demo.UnitTest.Command
{
    [TestClass]
    public class CommandInterpreterTests
    {
        [TestMethod]
        public void Class_InheritsFromInterface()
        {
            typeof(CommandInterpreter).Should().BeAssignableTo<ICommandInterpreter>();
        }

        [DataTestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("                            ")]
        public void ProcessInput_WhenInputIsNullOrEmptyOrWhiteSpace_ShouldThrowException(string command)
        {
            var commandInterpreter = new CommandInterpreter();
            Action processedInput = ()=> commandInterpreter.ProcessInput(command);

            processedInput.Should().Throw<ArgumentException>().WithMessage("Invalid command");
        }

        [TestMethod]
        public void ProcessInput_WhenInputIsValid_ShouldReturnListOfCommands()
        {
            var listOfCommands = new List<RoverCommand> {new RoverCommand(), new RoverCommand()};
            var commandInterpreter = A.Fake<ICommandInterpreter>();

            A.CallTo(() => commandInterpreter.ProcessInput(A<string>.Ignored)).Returns(listOfCommands);

            var processedInput = commandInterpreter.ProcessInput("cake with nutella");

            processedInput.Should().NotBeNull();
            processedInput.Should().HaveCount(2);
        }
    }
}
